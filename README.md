run this and point your domain in the proper IP address, so when running letsencrypt in manual mode, it will poll the domain and hit this app. Make sure to populate the ENV variable `LETSENCRYPT_RESPONSE` with what the expected response should be.

First run letsencrypt in manual mode:

```
./letsencrypt-auto certonly -a manual -d some.domain.com
```

It will then ask for admin password on your system, and ask if it is ok to log your IP as the requestor. Once accepted, then it will give you the expected URL, as well as the expected response. When Starting this container make sure to populate the env variable `LETSENCRYPT_RESPONSE` with the expected response.

Example:

```
docker run -d --restart=always -p 80:5000 -e "LETSENCRYPT_RESPONSE=expected.response" --name=letsencrypt-response skiftcreative/letsencrypt-response
```

First confirm now that the url does in fact respond with the proper response. When it does, hit [enter] in your terminal to tell it to make the request and you should now have your certs.

If you have ways to improve this, I am more than willing to take issues, and pull requests at

https://gitlab.com/skift/docker-letsencrypt-response

And it will be available over port 80. If you are using a proxy then adjust the ports in use accordingly to your setup.

It also has a docker image available to pull at

https://hub.docker.com/r/skiftcreative/letsencrypt-response/