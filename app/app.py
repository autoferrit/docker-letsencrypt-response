from flask import Flask, jsonify
import os

app = Flask(__name__)


@app.route('/')
def index():
    return 'Flask is running on gunicorn!'


@app.route('/.well-known/acme-challenge/<id>')
def names(id):
    return os.getenv('LETSENCRYPT_RESPONSE')

if __name__ == '__main__':
    app.run()